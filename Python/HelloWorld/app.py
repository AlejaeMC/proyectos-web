from datetime import date

#We check in a patient named John Smith
#He's 20 years old and is a new patient

full_name = "John Smith"
age = 20
is_new = True

#Inputs
name = input("What is your name? ")
print('Hi ' + name)

#Ask two questions: person's name and favourite color.
"Then, print a message like 'Mosh likes blue'"
PersonsName = input("What is your name? ")
FavColor = input("What is your favourite color? ")
print(PersonsName + ' likes ' + FavColor)

#Type conversion
birth_year = input('Birth year: ')
date = date.today()
print(type(birth_year))
currentAge = int(date.year) - int(birth_year)
print(type(currentAge))
print(type(birth_year))
print(currentAge)

#1 pound = 0.453592 kilograms
#or 1 Lb = 0.453592 Kg
#Ask a user their weight (in pounds), convert it to kilograms and print on the terminal
weightPounds = float(input('Your weight is?(Pounds) '))
weightKilograms = weightPounds *  0.453592
print(weightPounds, 'Pounds (Lbs) are equal to', weightKilograms, 'Kilograms (Kgs)')

course = 'Python for Beginners'
another = course[:]
print(another)
print(course[0:5])
print(course[0:]) #Starting string to the last index
print(course[:5]) #Starting from index 0 to index 5 (indicated one
print(course[:]) #Clone the string

name = 'Jennifer'
print(name[1:-1])
#ennife

first = 'John'
last = 'Smith'
message = first + ' [' + last + '] is a coder'
msg = f'{first} [{last}] is a coder' #Curly Braces
print(message)
print(msg)

#String methods
course = 'Python for Beginners'
print(len(course))
print(course.upper())
print(course.lower())
print(course)
print(course.find('O')) #Return -1 since we dont have O in the string
print(course.replace('Beginners', 'Absolute Beginners'))
print('Python' in course) #True

x = 2.9
print(round(x))
print(abs(-2.9))

import math
print(math.ceil(2.9))
print(math.floor((2.9)))

#python 3 math module

#if statements
is_hot = False
is_cold = True
if is_hot:
        print("It's a hot day")
elif is_cold:
        print("It's a cold day")
else:
        print("It's a lovely day")

#Price of a house is $1M, If buyer has a good credit they need to put down
#10% otherwise they need to put down 20%
house = 1000000
good_credit = True
if good_credit:
    down_payment = 0.1 * house
    print(down_payment)
    print(f"Down payment: ${down_payment}")
else:
    down_payment = 0.2 * house
    print(down_payment)

#Logical operators
has_high_income = True
has_good_credit = True

if has_high_income and has_good_credit: #AND - both should be true
    #Or: at least one should be true
    #Not: Inverses boolean value that recieves
    print("Eligible for loan")

#Comparisson operators
#if temperature +30 hot -10 cold otherwise neither hot nor cold
temperature = 30

if temperature > 30: #> >= < <= != === ==
        print("It's a hot day")
else:
        print("It's not a hot day")

#If name is less that 3 characters long
#name must be at leat 3 characters
#otherwise if it's more than 50 characters long
#name can be maximun of 50 characters
#otherwise
#name looks good
name = 'Ale'

if len(name) < 3:
    print('Name must be at least 3 characters')
elif len(name) > 50:
    print('Name can be maximun of 50 characters')
else:
    print('Name looks good')

#Project: Weight Converter
#Allow enter weight in any type (Pounds, Kilos) and then convert it to the another one
#Not case sensitve
weight = int(input('Weight: '))
unit = input('(L)bs or (K)g: ')
if unit.upper() == "L":
        converted = weight * 0.45
        print(f"You are {converted} kilos")
else:
        converted = weight / 0.45
        print(f"You are {converted} pounds")

"While loops"
#Guessin game
secret_number = 9
guess_count = 0
guess_limit = 3
while guess_count < guess_limit:
    guess = int(input('Guess: '))
    guess_count += 1
    if guess == secret_number:
        print('You won')
        break;
    else:
        print('Not the number, try again')
else:
    print('Sorry you failed')

#Car game (Simulation)
command = ""
started = False
while True:
    command = input(">").lower()
    if command == "start":
        if started:
            print("Car is already started!")
        else:
            started = True
            print("Car started...")
    elif command == "stop":
        if not started:
            print("Car is already stopped!")
        else:
            started = False
            print("Car stopped")
    elif command == "help":
        print("""
            start - to start the car
            stop - to stop the car
            quit - to quit
        """)
    elif command == "quit":
        break;
    else:
        print("Sorry. I don't understand that!")

#For loops
for item in 'Python':
    print(item)
for item in ['Mosh', 'John', 'Sarah']:
    print(item)
for item in (1,2,3,4,5,6,7,8,9,10):
    print(item)
for item in range(10):
    print(item)
for item in range(5, 10, 2):
    print(item)

#Calculate total cost of all items in a shopping cart
prices = [10,20,30]
total = 0
for price in prices:
    total += price
print(f"Total: {total}")

#Nested loops
for x in range(4):
    for y in range(3):
        print(f'({x}, {y})')

#Challenge
#numbers = [5, 2, 5, 2, 2]
numbers_list = []
seq = 0
while seq < 5:
    numbers = input('Give me a sequence of 5 numbers')
    numbers_list.append(numbers)
    seq += 1
    print(seq, 'Sequence')
    print(numbers_list, 'List')

#for x_count in int(numbers):
#    print('x' * x_count)

for x_count in numbers_list:
    output = ''
    for count in range(int(x_count)):
        output += 'x'
    print(output)

#Lists
names = ['John', 'Bob', 'Mosh', 'Sarah', 'Mary']
names[0] = 'John'
print(names[2:])
print(names)

#Write a program to dinf the largest number in a list
numbers = [1,2,3,4,5,6,7,8,9,10]
max = numbers[0]
for number in numbers:
    if number > max:
        max = number
print(max)

#2D Lists
matrix = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
]
print(matrix[0][2])

for row in matrix:
    for item in row:
        print(item)

#List methods
numbers = [5, 2, 2, 1, 7, 4]
numbers.insert(0, 10)
print(numbers)
#numbers.remove(5)
numbers.index(5) #Returns 0 (Index of the number where looking for)
numbers.pop()
print(2 in numbers) #Lookinf for 2 in the list numbers
print(numbers.count(2)) #Returns 2 times 2
numbers.sort() #Sorts the list
numbers.reverse() #Reverse the list
print(numbers)
numbers2 = numbers.copy() #Copy of the list
print(numbers2)
numbers.clear()

#Write a program to remove the duplicates in a list
listN = [2, 2, 3, 3, 4, 5, 6, 7, 8, 9, 10]
uniques = []
for number in listN:
    if number not in uniques:
        uniques.append(number)
print(uniques)

#Tuples
numberst = (1, 2, 3)
#numberst[0] = 10 Its inmutable

#Unpacking
coordinates = (1, 2, 3)
x, y, z = coordinates
print(x)

#Dictionaries
customer = {
    "name": "John Smith",
    "age": 30,
    "is_verified": True
}

print(customer["name"])
customer["name"] = "Jack Smith"
customer["birthdate"] = "Jan 1 1980"
print(customer)

#Excersie
phone = input("Phone: ")
digits_mapping = {
    "1": "One",
    "2": "Two",
    "3": "Three",
    "4": "Four"
}
output = ""
for ch in phone:
    output += digits_mapping.get(ch, "!") + " "
print(output)

#Emoji converter
message = input(">")
words = message.split(' ')
emojis = {
    ":)": "(❁´◡`❁)",
    ":(": "ಠ╭╮ಠ"
}
output = ""
for word in words:
    output += emojis.get(word, word) + " "
print(output)

#Functions
def greet_user():
    print('Hi there!')
    print('Welcome aboard')

print("Start")
greet_user()
print("Finish")

#Parameters
def greet_user(name):
    print(f'Hi {name}!')
    print('Welcome aboard')

print("Start")
#greet_user() Since the function requires a parameter, it's going to drop error
greet_user("John")
print("Finish")

#Keyword Arguments
#Improvee readability of the code

#Return statment
def square(number):
    return number * number

result = square(3)
print(result)
print(square(3))

#Creating a reusable fuction
def emoji_converter(message):
    words = message.split(' ')
    emojis = {
        ":)": "(❁´◡`❁)",
        ":(": "ಠ╭╮ಠ"
    }
    output = ""
    for word in words:
        output += emojis.get(word, word) + " "
    return output

message = input(">")
result = emoji_converter(message)
print(result)
print(emoji_converter(message))

#Exceptions
try:
    age = int(input('Age: '))
    income = 20000
    risk = income / age
    print(age)
except ZeroDivisionError:
    print('Age cannot be 0.')
except ValueError:
    print('Invalid value')

#Classes