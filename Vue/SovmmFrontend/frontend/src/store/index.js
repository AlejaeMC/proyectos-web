import Vue from 'vue';
import Vuex from 'vuex';
import Estudiantes from './Modules/Estudiantes';
import Maestros from './Modules/Maestros';
import AuthManager from './Modules/AuthManager';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    AuthManager,
    Estudiantes,
    Maestros,
  },
});
