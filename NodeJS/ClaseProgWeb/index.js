require("dotenv").config();
const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const passport = require("passport");
const JwtStrategy = require("passport-jwt").Strategy;
const ExtractJwt = require("passport-jwt").ExtractJwt;

const models = require("./configure-db");
const usuariosAPI = require("./routes/usuarios")(models);
const authenticationApi = require("./routes/authentication")(models);

app.set("views", "./views");
app.set("view engine", "pug");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

passport.use(new JwtStrategy({
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: process.env.SECRET_KEY
}, (payload, done)=>{
    const username = payload.username;
    if (!username) {
        return done("no hay usuario", null);
    } else {
        return done(null, username);
    }
} ));

app.use("/api/authentication", authenticationApi);
app.use("/api/usuarios", passport.authenticate("jwt", { session: false }), usuariosAPI);

app.all("/", (req, res)=>{
    res.render("index", {
        title: "Pagina de la API"
    });
});

const port = process.env.PORT || 3000;

app.listen(port, ()=> {
    console.log(`El servidor está ejecutandose en el puerto ${port}`);
});