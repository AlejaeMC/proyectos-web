const Sequelize = require("sequelize");

const sequelize = new Sequelize({
    dialect: 'sqlite',
    storage: 'database.sqlite'
});

sequelize.authenticate()
    .then(()=> {
        console.log("La conexión con la base de datos establecida");
    })
    .error((error) => {
        console.error(error);
    });

const Usuario = require("./models/usuario")(sequelize);

sequelize.sync();

module.exports = {
    sequelize,
    Usuario
}