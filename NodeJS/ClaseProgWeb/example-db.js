const Sequelize = require("sequelize");
const Model = Sequelize.Model;

const sequelize = new Sequelize({
    dialect: 'sqlite',
    storage: 'db.sqlite'
});

(async()=>{
    class Persona extends Model {}
    Persona.init({
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        nombre: Sequelize.STRING
    }, { sequelize, modelName:'persona' });

    class Pais extends Model {}
    Pais.init({
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        nombre: Sequelize.STRING
    }, {sequelize, modelName:'pais'});

    Pais.hasMany(Persona,{foreignKey:'paisId', as:'Personas'});
    Persona.belongsTo(Pais,{foreignKey:'paisId', as:'Pais'});
    Persona.belongsToMany(Persona,{through:'relations', as:'Amistades'});

    await sequelize.sync({force: true});

    /*const mexico = await Pais.create({
        nombre: 'Mexico'
    });
    const argentina = await Pais.create({
        nombre: 'Argentina'
    });*/

    const paises = await Pais.bulkCreate([
        { nombre: 'Mexico' },
        { nombre: 'Argentina'}
    ]);

    const mexico = paises[0];

    const persona = await Persona.create({
        nombre: 'Francisco'
    });

    const persona2 = await Persona.create({
        nombre: 'Raul'
    });

    const persona3 = await Persona.create({
        nombre: 'Angelica',
        paisId: 2
    });

    //await mexico.setPersonas([persona, persona2]);
    await persona.setPais(mexico);

    await persona.setAmistades([persona2, persona3]);

    await persona.getAmistades().map(persona => {
        console.log(persona.nombre);
    });

    await mexico.getPersonas().map(persona => {
        console.log(persona.nombre);
    })

    //console.log(await persona.getPais());

})();