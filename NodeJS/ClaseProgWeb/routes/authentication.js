const routes = require("express").Router();
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

module.exports = (models) => {

    routes.post("/register", async (req, res) => {
        const userRegister = req.body;
        const user = await models.Usuario.findOne(
            {where: { username: userRegister.username} }
        );

        if (!user) {
            const hash = await bcrypt.hash(userRegister.password, +process.env.SALT_ROUNDS);
            userRegister.password = hash;

            await models.Usuario.create(userRegister);
            res.send({ message: 'ok' })
        } else {
            res.status(403).send({
                message: 'User already exists'
            });
        }
    });

    const generateToken = (user) => new Promise((resolve, reject) => {
        try {
            const token = jwt.sign({
                username: user.username,
                lastname: user.lastname
            }, process.env.SECRET_KEY);
    
            resolve(token);
        } catch (error) {
            reject(error);
        }
    });

    routes.post("/login", async(req, res)=> {
        const wrongMessage = { 
            message: 'User or password is wrong'
        };

        const userLogin = req.body;

        const user = await models.Usuario.findOne({ where: { username: userLogin.username}});

        if (user) {
            const result = await bcrypt.compare(userLogin.password, user.password);
            if (result) {
                const token = await generateToken(user);

                res.send({ token });
            } else {
                res.status(403).send(wrongMessage);
            }
        }else {
            res.status(403).send(wrongMessage);
        }
    });

    return routes;
}