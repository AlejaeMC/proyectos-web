const routes = require("express").Router();

module.exports = (models) => {

    routes.get("/", async (req, res)=> {
        let usuarios = await models.Usuario.findAll({
            attributes: ["username", "name", "lastname", "age", "country"]
        });

        /*usuarios = usuarios.map(usuario => {
            return {
                username: usuario.username,
                name: usuario.name,
                lastname: usuario.lastname,
                age: usuario.age,
                country: usuario.country
            }
        })*/

        res.send(usuarios);
    });

    routes.get("/:id", async (req, res)=> {
        const idUser = req.params.id;

        const usuario = await models.Usuario.findOne({
            attributes: ["username", "name", "lastname", "age", "country"],
            where: {
                id: idUser
            }
        });

        res.send(usuario);
    });    

    return routes;
}