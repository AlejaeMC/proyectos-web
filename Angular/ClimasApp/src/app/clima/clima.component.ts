import { Component, OnInit } from '@angular/core';
import { GeolocationService } from '../geolocation.service';

@Component({
  selector: 'app-clima',
  templateUrl: './clima.component.html',
  styleUrls: ['./clima.component.css']
})
export class ClimaComponent implements OnInit {

  dataseries: any = [];
  loaded = false;
  climaHoy: any = [];
  cityInfo: any = [];

  constructor(private geolocation: GeolocationService) {}

  ngOnInit() {
    this.geolocation.getInfo().subscribe(
      (data: any) => {
        console.log(data);
        this.loaded = true;
        this.climaHoy = data.clima
        this.cityInfo = data.info
      }, error => {
        alert('Ocurrio un error');
        console.log(error);
      }
    );
  } 

}
