import { Component, OnInit } from '@angular/core';
import { GeolocationService } from '../geolocation.service';

@Component({
  selector: 'app-fecha',
  templateUrl: './fecha.component.html',
  styleUrls: ['./fecha.component.css']
})
export class FechaComponent implements OnInit {

  cityInfo: any = [];  
  loaded = false;
  d= new Date();

  constructor(private geolocation: GeolocationService) {}

  ngOnInit() {
    this.geolocation.getInfo().subscribe(
      (data: any) => {
        console.log(data);
        this.loaded = true;
        this.cityInfo = data.info
      }, error => {
        alert('Ocurrio un error');
        console.log(error);
      }
    );
  } 

}

