import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { RoutesModule } from './routes/routes.module';
import { ClimaComponent } from './clima/clima.component';
import { FechaComponent } from './fecha/fecha.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    ClimaComponent,
    FechaComponent
],
  imports: [
    BrowserModule,
    RoutesModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
