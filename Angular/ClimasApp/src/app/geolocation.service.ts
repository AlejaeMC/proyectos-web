import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { flatMap, map, mergeMap, concatMap, concatAll } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GeolocationService {

  private tokenGeoLocation = '2ea8479d9fc86551e1e423a712935a22';
  private baseUrl = 'http://api.ipstack.com/check';
  
/*   private baseUrl = 'http://api.ipstack.com/177.228.108.40'; */

  constructor(private http: HttpClient) { }

  formatdate(s :string){
    var year=s.substring(0,3);
    return year
  }

  getInfo() :Observable<any>  {
    const params = new HttpParams() 
      .set('access_key', this.tokenGeoLocation);

      return this.http.get(this.baseUrl, {
        params
      }).pipe(
        mergeMap(
          (info: any) => {
            const params = {
              lon: info.longitude,
              lat: info.latitude,
              product: 'civillight',
              output: 'json'
            };
            return this.http.get('http://www.7timer.info/bin/api.pl', {params})
              .pipe(
                map(clima => ({clima, info}))
              );
          }
        )
      );
  }

}
