import { Component, OnInit } from '@angular/core';
import { RickandmortyService } from '../rickandmorty.service';
import { NgbModal, NgbPopoverModule } from '@ng-bootstrap/ng-bootstrap';
import { InfoLocationComponent } from '../info-location/info-location.component';

@Component({
  selector: 'app-rickandmorty',
  templateUrl: './rickandmorty.component.html',
  styleUrls: ['./rickandmorty.component.css']
})


export class RickandmortyComponent implements OnInit {


  info: any = [];
  Algunarreglo = [];
  
  constructor(private rickAndMortyAPI: RickandmortyService, public modalService: NgbModal, public popOver: NgbPopoverModule) { }

  ngOnInit() {
  }

  cargarDatos(input){
    let cantidad = input || 4;
    if(cantidad < 1)
      alert('Introduzca un valor mayor a 0');
    else
      this.obtenerDatos(cantidad);
  }

  obtenerDatos(cantidad){
    this.rickAndMortyAPI.obtenerMultiplesPersonajes(cantidad).subscribe(
      data => {
        if(Array.isArray(data))
          this.info=data;
        else
          this.info.push(data)
          console.log(data);
          console.log(this.Algunarreglo)
          console.log(this.info)
      },
      error => {
        alert(error);
      }
      
    )
  }

  open(url) {
    const componentRef = this.modalService.open(InfoLocationComponent);
    componentRef.componentInstance.url = url;
  }


}
