import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-tablerandom',
  templateUrl: './tablerandom.component.html',
  styleUrls: ['./tablerandom.component.css']
})
export class TablerandomComponent implements OnInit {

  columns = new FormControl();
  rows = new FormControl();
  arr: any;

  ngOnInit(): void {
    this.columns.setValue('5');
    this.rows.setValue('10');
    this.crearTabla();
  }

  crearTabla() {
    const rows = Number.parseInt(this.rows.value);
    const columns = Number.parseInt(this.columns.value);
    this.arr = new Array(rows)
      .fill(0)
      .map(() => new Array(columns)
        .fill(0)
        .map(() => Math.floor(Math.random()*101))
      );
  }

  actualizarTabla(event) {
    this.crearTabla();
  }

}
