﻿using ControlIngresoGasto.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ControlIngresoGasto.Data
{
    public class ApplicationDbContext : DbContext
    {

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> option) : base(option)
        {



        }

        public DbSet<Categoria> Categorias { get; set; }

        public DbSet<IngresoGasto> IngresoGastos { get; set; }

    }
}
