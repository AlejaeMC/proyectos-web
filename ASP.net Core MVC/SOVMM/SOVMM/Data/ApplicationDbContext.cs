﻿using Microsoft.EntityFrameworkCore;
using SOVMM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SOVMM.Data
{
    public class ApplicationDbContext : DbContext
    {

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> option) : base(option)
        {



        }

        public DbSet<User> Users { get; set; }

    }
}
