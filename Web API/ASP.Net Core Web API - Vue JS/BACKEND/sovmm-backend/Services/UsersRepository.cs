﻿﻿using System;
using System.Collections.Generic;
 using System.IdentityModel.Tokens.Jwt;
 using System.Linq;
 using System.Security.Claims;
 using System.Text;
 using Microsoft.IdentityModel.Tokens;
 using MongoDB.Bson;
using MongoDB.Driver;
using sovmm_backend.Models;

namespace sovmm_backend.Services
{
    public interface IUsersRepository
    {
        public User Get(string id);
        public User New(User newUser);
        public User Login(AuthModel authModel);
        public List<User> Get();
        User GetByEmail(string email);
        User GetByACLVL(int accesLevel);
        public void Update(string id, User updatedUser);
        public void Delete(string id);
    }
    public class UsersRepository : IUsersRepository
    {
        private static Global global = new Global();
        private readonly IMongoCollection<User> _mUsers;
        private readonly string _secretCredentials;
        public UsersRepository(IConnSettings connSettings)
        {
            var mongoClient = new MongoClient(connSettings.ConnUrl);
            var dB = mongoClient.GetDatabase(connSettings.DbName);
            _mUsers = dB.GetCollection<User>(connSettings.UserCollectionName);
            _secretCredentials = connSettings.SecretCredentials;

        }
        public User Get(string id)
        {
            return _mUsers.Find(user => user.Id == id).FirstOrDefault();
        }
        public User New(User newUser)
        {
            var user = _mUsers.Find(user => user.Email.ToLower() == newUser.Email.ToLower()).FirstOrDefault();
            if (user == null)
            {
                _mUsers.InsertOne(newUser);
                return newUser;
            }

            return null;
        }
        public List<User> Get() => _mUsers.Find(user => true).ToList();
        public void Update(string id, User updatedUser)
        {
            _mUsers.ReplaceOne(user => user.Id == id, updatedUser);
        }
        public void Delete(string id)
        {
            _mUsers.DeleteOne((user => user.Id == id));
        }
        public User GetByEmail(string email)
        {
            return _mUsers.Find(user => user.Email.ToLower().Contains(email.ToLower())).FirstOrDefault();
        }
        public User GetByACLVL(int accesLevel)
        {
            return _mUsers.Find(user => user.AccessLevel == accesLevel).FirstOrDefault();
        }
        public User Login(AuthModel authModel)
        {
            var email = authModel.Email;
            var password = authModel.Password;
            var mobile = authModel.FromMobile;
            var user = _mUsers.Find(user => user.Email.ToLower() == email.ToLower()).FirstOrDefault();


            if (user == null)
            {
                User usr = new User();
                usr.Email = "ENF";
                return usr;
            }
            
            if (user.Password != password)
            {
                User usr = new User();
                usr.Password= "PNF";
                return usr;
            }
            
            var authHelper = new AuthHelper();
            var credentials = new UserCredentials();
            var expirationDays = 7;
            if (mobile)
            {
                expirationDays = 365;
            }
            credentials.Token = authHelper.GetJwtToken(user.Email, _secretCredentials, expirationDays);
            credentials.ExpirationInDays = expirationDays;
            user.Credentials = credentials;
            return user;
        }

    }
}
