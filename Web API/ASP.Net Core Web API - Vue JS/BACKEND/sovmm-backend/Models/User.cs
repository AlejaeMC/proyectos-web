﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using sovmm_backend.Models;

namespace sovmm_backend.Models
{
    [BsonIgnoreExtraElements]
    public class User
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        [BsonIgnoreIfDefault]
        public string Id { get; set; }
        [MaxLength(250)]
        public string Image { get; set; }
        [Required]
        [Display(Name = "Correo electronico institutocional (Tecnologico de Culiacan)")]
        public string Email { get; set; }
        [Required]
        [Display(Name = "Contraseña")]
        public string Password { get; set; }
        [Required]
        [Display(Name = "Correo electronico personal")]
        public string EmailPersonal { get; set; }
        [Required]
        [MaxLength(40)]
        [Display(Name = "Nombre Completo")]
        public string FirstName { get; set; }
        [Required]
        [MaxLength(40)]
        [Display(Name = "Apellido Completo")]
        public string LastName { get; set; }
        [Display(Name = "Ciudad")]
        public string City { get; set; }
        [Display(Name = "Estado")]
        public string State { get; set; }
        [Required]
        [Display(Name = "CURP")]
        public string Curp { get; set; }
        [Required]
        [Display(Name = "Telefono")]
        public string Phone { get; set; }
        [Display(Name = "Direccion")]
        public string Address { get; set; }
        [Required]
        [Display(Name = "Carrera")]
        public string Career { get; set; }
        [Required]
        [MaxLength(8)]
        [Display(Name = "Numero de control")]
        public int ControlNumber { get; set; }
        [Required]
        [Display(Name = "Departamento")]
        public string Department { get; set; }
        [Required]
        [Display(Name = "Fecha de nacimiento")]
        public DateTime BirthDate { get; set; }
        [Required]
        [Display(Name = "Fecha de registro de usuario")]
        public DateTime UserSince { get; set; }
        [Required]
        [Display(Name = "Estatus")]
        public bool Status { get; set; }
        [Required]
        public int AccessLevel { get; set; }
        public UserCredentials Credentials { get; set; }
    
        public override string ToString()
        {
            return $"{Id}, {Email}, {Password}";
        }
    }
}

public class UserCredentials
{
    public string Token { get; set; }
    public int ExpirationInDays { get; set; }
}
public class AuthModel
{
    [Required]
    public string Email { get; set; }
    [Required]
    public string Password { get; set; }
    public bool FromMobile { get; set; }
}