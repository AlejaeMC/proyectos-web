import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
  {
    path: '/mi-directorio',
    name: 'MiDirectorio',
    components: {
      default: () => import('@/views/MiDirectorio.vue'),
      nav: () => import('@/components/Global/Nav'),
    },
  },
  {
    path: '/',
    name: 'Landing',
    component: () => import('@/views/Login.vue'),
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('@/views/Login.vue'),
  },
  {
    path: '/inicio',
    name: 'Inicio',
    components: {
      default: () => import('@/views/Inicio.vue'),
      nav: () => import('@/components/Global/Nav'),
    },
  },
  {
    path: '/test',
    name: 'Test',
    components: {
      default: () => import('@/views/Test.vue'),
      //  nav: () => import('@/components/Global/Nav.vue'),
    },
  },
  {
    path: '/proyectos-residencias',
    name: 'Proyectos-Residencias,',
    components: {
      default: () => import('@/views/ProyectosResidencias.vue'),
      nav: () => import('@/components/Global/Nav'),
    },
  },
  {
    path: '/mi-proyecto-residencias',
    name: 'Proyecto-de-Residencias,',
    components: {
      default: () => import('@/views/MiProyectoResidencias.vue'),
      nav: () => import('@/components/Global/Nav'),
    },
  },
  {
    path: '/alumnos',
    name: 'Alumnos',
    components: {
      default: () => import('@/views/Alumnos.vue'),
      nav: () => import('@/components/Global/Nav'),
    },
  },
  {
    path: '/maestros',
    name: 'Maestros',
    components: {
      default: () => import('@/views/Maestros.vue'),
      nav: () => import('@/components/Global/Nav'),
    },
  },
  {
    path: '/cuenta',
    name: 'cuenta',
    components: {
      default: () => import('@/views/Cuenta.vue'),
      nav: () => import('@/components/Global/Nav.vue'),
    },
  },
  {
    path: '/cuentaM',
    name: 'cuentaM',
    components: {
      default: () => import('@/views/CuentaM.vue'),
      nav: () => import('@/components/Global/Nav.vue'),
    },
  },
  {
    path: '/admin/nm',
    name: 'nuevo-maestro',
    components: {
      default: () => import('@/views/NuevoMaestro'),
      nav: () => import('@/components/Global/Nav.vue'),
    },
  },
  {
    path: '/admin/na',
    name: 'nuevo-alumno',
    components: {
      default: () => import('@/views/NuevoAlumno'),
      nav: () => import('@/components/Global/Nav.vue'),
    },
  },
  {
    path: '*',
    component: () => import('@/views/NotFound'),
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
