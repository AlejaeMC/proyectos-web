import Axios from 'axios';
import Vue from 'vue';
import JsonExcel from 'vue-json-excel';
import JwPagination from 'jw-vue-pagination';
import App from './App.vue';
import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';

// register jw pagination component globally
Vue.component('jw-pagination', JwPagination);

Vue.component('downloadExcel', JsonExcel);

Vue.config.productionTip = false;

// eslint-disable-next-line import/prefer-default-export
export const testEnv = false;
// Axios.defaults.baseURL = 'http://34.122.173.95:500';
Axios.defaults.baseURL = 'http://localhost:5000';

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount('#app');
