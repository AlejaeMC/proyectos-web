import Axios from 'axios';
import AuthManager from '@/store/modules/AuthManager';

function compare(a, b) {
  // Use toUpperCase() to ignore character casing
  const repA = a.created_by.length;
  const repB = b.created_by.length;
  console.log(a, b);
  let comparison = 0;
  if (repA > repB) {
    comparison = -1;
  } else if (repA < repB) {
    comparison = 1;
  }
  return comparison;
}

const state = {
  presidencias: [],
  proyectos: [],
  myPresidencias: [],
  myProyectos: [],
};
const getters = {
  getPresidencias(state) {
    return state.presidencias.filter((v) => v.status === 1).sort(compare);
  },
  getPresidenciasRev(state) {
    return state.presidencias.filter((v) => v.status === 0);
  },
  getPresidenciasAll(state) {
    return state.presidencias;
  },
  /* eslint-disable consistent-return */
  getPresidenciasByUser(state) {
    const useer = AuthManager.state.user;
    if (useer.departamento === 'Departamento de Sistemas y Computacion') {
      return state.presidencias.filter((fil) => fil.departamento === 'Departamento de Sistemas y Computacion');
    }
    if (useer.departamento === 'Departamento de Metal-Mecanica') {
      return state.presidencias.filter((fil) => fil.departamento === 'Departamento de Metal-Mecanica');
    }
  },
  getPresidenciasCategoria01(state) {
    console.log(state.presidencias, 'STATE');
    return state.presidencias.filter((value) => value.categoria === 'Industria');
  },
  getPresidenciasCategoria02(state) {
    return state.presidencias.filter((value) => value.categoria === 'Web');
  },
  getPresidenciasCategoria03(state) {
    return state.presidencias.filter((value) => value.categoria === 'Aplicacion Android');
  },
  getPresidenciasCategoria04(state) {
    return state.presidencias.filter((value) => value.categoria === 'Redes');
  },
  getPresidenciasFilterTodo(state) {
    const useer = AuthManager.state.user;
    return state.presidencias.filter((value) => value.departamento === useer.departamento);
  },
  getPresidenciasFilter0(state) {
    const useer = AuthManager.state.user;
    return state.presidencias.filter((value) => value.status === 0
      && value.departamento === useer.departamento);
  },
  getPresidenciasFilter1(state) {
    const useer = AuthManager.state.user;
    return state.presidencias.filter((value) => value.status === 1
      && value.departamento === useer.departamento);
  },
  getPresidenciasFilter2(state) {
    const useer = AuthManager.state.user;
    return state.presidencias.filter((value) => value.status === 2
      && value.departamento === useer.departamento);
  },
  getPresidenciasFilter3(state) {
    const useer = AuthManager.state.user;
    return state.presidencias.filter((value) => value.status === 3
      && value.departamento === useer.departamento);
  },
  getMyPresidencias(state) {
    return state.myPresidencias;
  },
  getProyectos(state) {
    return state.proyectos;
  },
  getMyProyectos(state) {
    return state.myProyectos;
  },
  getMyProyectoID(state) {
    console.log(state.myPresidencias.id, 'IDDD');
    return state.myPresidencias.proyectoId;
  },
  getPresidenciasUnfiltered(state) {
    return state.presidencias;
  },
  getPresidenciasFiltered(state, filter) {
    return state.presidencias.filter(((value) => value === filter));
  },
};
const mutations = {
  setPresidencias(state, presidencias) {
    state.presidencias = presidencias;
  },
  setMyPresidencias(state, presidencias) {
    state.myPresidencias = presidencias;
  },
  setProyectos(state, proyectos) {
    state.proyectos = proyectos;
  },
  setMyProyectos(state, proyectos) {
    state.myProyectos = proyectos;
  },
};
const actions = {
  fetchPresidencias(context) {
    return new Promise(((resolve) => {
      Axios.get('presidencias', {
        headers: {
          Authorization: `Bearer ${context.getters.getToken}`,
        },
      }).then((res) => {
        resolve(true);
        context.commit('setPresidencias', res.data);
      });
    }));
  },
  fetchMyPresidencias(context) {
    Axios.get(`presidencias/user/${context.getters.getUserId}`, {
      headers: {
        Authorization: `Bearer ${context.getters.getToken}`,
      },
    }).then((res) => {
      context.commit('setMyPresidencias', res.data);
      // console.log(res.data, 'hola');
    });
  },
  fetchProyectos(context) {
    Axios.get('proyectos', {
      headers: {
        Authorization: `Bearer ${context.getters.getToken}`,
      },
    }).then((res) => {
      context.commit('setProyectos', res.data);
      // console.log(res.data, 'hola2');
    });
  },
  fetchMyProyectos(context) {
    Axios.get(`proyectos/presidencias/${context.getters.getMyProyectoID}`, {
      headers: {
        Authorization: `Bearer ${context.getters.getToken}`,
      },
    }).then((res) => {
      context.commit('setMyProyectos', res.data);
      // console.log(res.data, 'hola');
    });
  },
  updatePresidencias(context, updatedPresidencias) {
    Axios.put(`presidencias/${updatedPresidencias.id}`, updatedPresidencias, {
      headers: {
        Authorization: `Bearer ${context.getters.getToken}`,
      },
    }).then(() => {
      context.dispatch('fetchPresidencias');
    });
  },
  updateProyectos(context, updatedProyectos) {
    Axios.put(`proyectos/${updatedProyectos.id}`, updatedProyectos, {
      headers: {
        Authorization: `Bearer ${context.getters.getToken}`,
      },
    }).then(() => {
      context.dispatch('fetchProyectos');
    });
  },
};

export default {
  state, getters, mutations, actions,
};
