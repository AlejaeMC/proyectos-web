import Vue from 'vue';
import Vuex from 'vuex';
import ResidenciasManager from '@/store/modules/ResidenciasManager';
import UserManager from '@/store/modules/UserManager';
import AuthManager from '@/store/modules/AuthManager';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    AuthManager,
    ResidenciasManager,
    UserManager,
  },
});
