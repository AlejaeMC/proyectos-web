php -S 127.0.0.1:8000 Iniciar servicio PHP en consola para poder correr archivos PHP

<?php   

        echo "<br>";
        echo "<br>";
        echo "Hello World";
        echo 25 + 70 
        // Single line comments
        # Single line comments
        /**
            *Comments
        */

        /**
         * Data types
         *  String
         *  Int
         *  Float (Floating point numbers - also called double)
         *  Boolean
         *  Array   
         *  Object
         *  Null
         *  Resource
         */

         // Variables
         

        /** 
         *  Start with a $
         *  Followed by letter or _underscored
         *  Contain letters, numbers, underscores, dashes
         *  No spaces
         *  Case sensitive
        */
?>

<?php
    //Outputting variable data
    $num1 = 75;
    $num2 = 25;
    $result = $num1 + $num2;

    echo $result;
    echo "<p>";
?>

<?php

    $text1 = "Hello";
    $text2 = "World";
    
    echo $text1 . ' ' . $text2;
    echo "<p>";

?>

<?php
    $text = "Hello world";

    $output_data = "{$text} is a \"simple\" string";
    $output_data2 = "$text is a simple string25";
    echo $output_data;
    echo "<p>";
    echo "&nbsp";
    echo $output_data2;
    echo "<p>";
?>

<?php
    $a = array(1, 2, array("a", "b", "c"));
    var_dump($a);
    echo "<p>";
?>

<?php

define('MIN_VALUE', '0.0');   // RIGHT - Works OUTSIDE of a class definition.
define('MAX_VALUE', '1.0');   // RIGHT - Works OUTSIDE of a class definition.

//const MIN_VALUE = 0.0;         RIGHT - Works both INSIDE and OUTSIDE of a class definition.
//const MAX_VALUE = 1.0;         RIGHT - Works both INSIDE and OUTSIDE of a class definition.

class Constants
{
  //define('MIN_VALUE', '0.0');  WRONG - Works OUTSIDE of a class definition.
  //define('MAX_VALUE', '1.0');  WRONG - Works OUTSIDE of a class definition.

  const MIN_VALUE = 0.0;      // RIGHT - Works INSIDE of a class definition.
  const MAX_VALUE = 1.0;      // RIGHT - Works INSIDE of a class definition.

  public static function getMinValue()
  {
    return self::MIN_VALUE;
  }

  public static function getMaxValue()
  {
    return self::MAX_VALUE;
  }
}

?>

<?php 
    $numero1 = 20.0;

    echo var_dump( is_int($numero1));
    echo "<p>";

    echo(min(0, 150, 600, 30, -40, -90)); #Es igual con el max

    echo "<p>";

    echo date("D, M, Y");
    echo "&nbsp";
    echo date("d, M, Y");
    echo "<p>Hello &nbsp; punt";
    // This will render as Hello   Punt (with 4 white spaces)

?>

<?php

    include 'nav.php';
    
    //Specify property for the file
    require 'conn.php';
?>

<pre></pre>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form method="POST" action="">
        <table>
            <tr>
                <td>Username: </td>
                <td><input type="text" name="u_name"></td>
            </tr>
            <tr>
                <td>Password: </td>
                <td><input type="password" name="u_pass"></td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" name="u_login"></td>
            </tr>
        </table>
    </form>

    <?php
        if ( isset($_POST['u_login']) ){
            $u_name = $_POST['u_name'];
            $u_pass = md5($_POST['u_pass']);

            echo $u_name;
            echo '<br>';
            echo $u_pass;

        }
    ?>
</body>
</html>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        $server_name = 'localhost';
        $user_name = 'root';
        $pass_word = 'root';
        $db = 'demo';

        //$conn = mysqli_connect( $server_name, $user_name, $pass_word, $db); One way to connect to MYSQL database
        $conn = mysqli_connect( 'localhost', 'root', 'root', 'demo'); //Another way

        if( !$conn ){
            die( 'Unable to connect' );
        }else{
            echo '<br>';
            echo '<br>';
            echo 'Connected';
        }
    ?>
</body>
</html>