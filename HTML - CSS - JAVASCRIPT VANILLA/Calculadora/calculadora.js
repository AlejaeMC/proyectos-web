// FUNCIONES PARA OBTENER NUMEROS Y OPERADORES PARA MOSTRAR EN LA PANTALLA
function getHistorial()
{
	return document.getElementById("valor-historial").innerText;
}

function printHistorial(numero)
{
	document.getElementById("valor-historial").innerText=numero;
}

function getSalida()
{
	return document.getElementById("output").innerText;
}

function printSalida(numero)
{

	if(numero=="")
	{
		document.getElementById("output").innerText=numero;
	}else
	{
		// Si numero no es vacio y tiene comilla o coma, lo formatea para poder usarse en caso de querer seguir haciendo operaciones
		document.getElementById("output").innerText=getFormattedNumber(numero);
	}	
}

function getFormattedNumber(numero)
{
	if(numero=="-")
	{
		return "";
	}

	var numersin = Number(numero);
	var value = numersin.toLocaleString("en");
	// Regresa una cadena enfocada al idioma
	return value;
}

function reverseNumberFormat(numero)
{
	return Number(numero.replace(/,/g,''));
	// Quita comillas y comas
}

// TERMINAN FUNCIONES PARA OBTENER NUMEROS Y OPERADORES EN PARTE DE RESULTADO

// JALA LOS NUMEROS ASIGNADOS EN index.html con la clase "numero"
var number = document.getElementsByClassName("numero");

for(var i =0;i<number.length;i++)
{
	
	number[i].addEventListener('click',function()
	// QUE NUMERO CLICKEAS LO DETECTA Y BUSCA EN EL RECORRIDO
	{
	
		var output=reverseNumberFormat(getSalida());
		// Consigue el valor y lo formatea

		if(output!=NaN)
		// Si es diferente de NaN es porque es un numero
		{
			output=output+this.id;
			// Asigna a output el valor de salida jalandolo con el ID
			printSalida(output);
		}
	});
}

// TERMINA NUMEROS

// Jala los operadores que se introduzcan
var operador = document.getElementsByClassName("operador");

for(var i =0;i<operador.length;i++)
{

	operador[i].addEventListener
	('click',function()
	{
	
		// AQUI APLICA CE
		if(this.id=="limpiar")
		{
		
			printHistorial("");
			printSalida("");
			
		
		}
		// Aqui el del (eliminar numero por numero)
		else if(this.id=="backspace")
		{

			var salida=reverseNumberFormat(getSalida()).toString();
			if(salida)
			{
			
				salida = salida.substr(0,salida.length-1);
				printSalida(salida);
			
			}
		
		// DE OTRA MANERA AGARRA LOS NUMEROS TECLEADOS U OPERADORES
		}else
		{
		
			var salida=getSalida();
			var historial=getHistorial();

			// Verifica que output tenga valor e historial tenga valor
			if(salida =="" && historial!="")
			{
			
				if(isNaN(historial[historial.length-1]))
				// Verifica que no sea valor NaN
				{

					historial = historial.substr(0,historial.length-1);
				
				}
			
			}
			
			if(salida!="" || historial!="")
			// Aqui pregunta si ya hay valores
			{
			
				salida = salida == "" ? salida: reverseNumberFormat(salida);
				historial=historial+salida;
				
				if(this.id=="=")
				// Aqui evalua las operaciones que hay en el historial sea 1 o sea mas de 1 siempre y cuando se presione =
				{
				
					var resultado = eval(historial);
					printSalida(resultado);
					// Muestra resultado en pantalla
					printHistorial("");
					//Muestra historial en vacio
				
				}else
				// De otra manera son operadores o numeros
				{
				
					historial=historial+this.id;
					printHistorial(historial);
					// Muestra valores en pantalla de historial
					printSalida("");
					// En salida no muestra nada
				
				}
			
			}
		
		}
		
	});

}